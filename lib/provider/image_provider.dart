import 'package:flutter/foundation.dart';

class ImagePathProvider extends ChangeNotifier {
  String? imagepath;
  void selectImage(String? value) {
    imagepath = value;

    notifyListeners();
  }
}
