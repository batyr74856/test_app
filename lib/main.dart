import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_app/widgets/buttons/default_button.dart';

import 'provider/image_provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => ImagePathProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                DefaultButton(
                  color: const Color.fromRGBO(241, 86, 55, 1),
                  label: 'Show meeting',
                  textColor: Colors.white,
                  onPressed: () {
                    Provider.of<ImagePathProvider>(context, listen: false)
                        .selectImage('assets/images/buisness.png');
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                DefaultButton(
                  color: const Color.fromRGBO(64, 64, 66, 1),
                  label: 'Show idea',
                  textColor: Colors.white,
                  onPressed: () {
                    Provider.of<ImagePathProvider>(context, listen: false)
                        .selectImage('assets/images/Climb.png');
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                DefaultButton(
                  color: const Color.fromRGBO(244, 186, 22, 1),
                  label: 'Show artists',
                  textColor: Colors.white,
                  onPressed: () {
                    Provider.of<ImagePathProvider>(context, listen: false)
                        .selectImage('assets/images/favority.png');
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                ImagePathWidget(),
              ],
            ),
          ),
        ));
  }
}

class ImagePathWidget extends StatelessWidget {
  const ImagePathWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    String? selectedImagePath =
        Provider.of<ImagePathProvider>(context).imagepath;
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Image.asset(
        selectedImagePath ?? 'assets/images/Climb.png',
        fit: BoxFit.fill,
      ),
    );
  }
}
