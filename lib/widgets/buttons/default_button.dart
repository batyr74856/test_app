import 'package:flutter/material.dart';

class DefaultButton extends StatelessWidget {
  final Color color;
  final Color textColor;
  final String label;
  final Widget? child;
  final void Function()? onPressed;
  final bool loading;
  final double borderRadius;
  final double fontSize;

  DefaultButton(
      {Key? key,
      this.color = Colors.white,
      this.textColor = Colors.black,
      this.onPressed,
      this.child,
      this.loading = false,
      this.borderRadius = 8,
      this.fontSize = 16,
      this.label = ''})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (loading) {
          return;
        }
        if (onPressed != null) {
          onPressed!();
        }
      },
      child: TextButton(
        style: ButtonStyle(
          padding: MaterialStateProperty.all(
              const EdgeInsets.symmetric(horizontal: 25, vertical: 10)),
          backgroundColor: MaterialStateProperty.all<Color>(color),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(borderRadius))),
        ),
        onPressed: onPressed,
        child: loading
            ? const Center(
                child: SizedBox(
                  width: 15,
                  height: 15,
                  child: CircularProgressIndicator(
                    color: Colors.white,
                  ),
                ),
              )
            : child ??
                Center(
                  child: Wrap(
                    children: [
                      Text(
                        label,
                        style: TextStyle(
                            color: textColor,
                            fontWeight: FontWeight.w900,
                            fontSize: fontSize),
                      )
                    ],
                  ),
                ),
      ),
    );
  }
}
